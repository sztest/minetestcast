# MineTestCast: Livestreaming Minetest in Docker

This docker image encapsulates a Minetest client which automatically stays connected to a server, with ffmpeg, which captures the audio+video output and livestreams it to a chosen destination.  It runs fully headless with no physical display/audio/input.  Each component (stream, client) is restarted if it exits while the container is still running.

Since there is no input for the client, for this to be useful, you will probably need to pre-configure the client separately, and use some mods to control the player's position as a camara (e.g. [szutil_cinecam](https://gitlab.com/sztest/szutilpack/-/tree/master/szutil_cinecam)), and possibly to render the player invisible (e.g. [szutil_stealth](https://gitlab.com/sztest/szutilpack/-/tree/master/szutil_stealth)).

## Starting

You can run the image from Docker Hub:

*(N.B. you will have to remove the comment and blank lines for this to be a valid shell script)*

```sh
docker exec -d --restart=always \

	# ======== BASIC SETTINGS ========

	# Custom resolution
	-e width=1024 \
	-e height=576 \

	# Minetest server login info
	-e mtserver=172.17.0.1 \
	-e mtport=30000 \
	-e mtuser=stream \
	-e mtpass=stream \

	# Stream destination
	-e outformat=flv \
	-e outuri=rtmp://172.17.0.1/live \

	# ======== FINE-TUNING ========

	# Video quality (https://trac.ffmpeg.org/wiki/Encode/H.264#crf)
	-e vcrf=32 \
	# Video average bitrate limit
	-e vmaxrate=800k \
	# Video bitrate burst tolerance
	-e vmaxburst=1600k \

	# Audio VBR quality
	-e avbr=2 \

	# Network timeout (seconds)
	-e fftimeout=10 \

	# Target frames per second
	-e fps=30 \
	# Keyframe interval in frames, should not exceed ~2*fps
	-e vkeyint=60 \

	# Override background (stand by) and overlay (logo)
	-v `pwd`/backdrop.png:/app/backdrop.png:ro \
	-v `pwd`/overlay.png:/app/overlay.png:ro \

	# Custom minetest config file
	# Overrides built-in default settings
	# N.B. screen_w/screen_h/max_fps overridden
	-v `pwd`/minetest.conf:/app/minetest.conf:ro \

	# Custom commands to run in the container immediately
	# before attempting to start publishing the stream
	-v `pwd`/pre-ffmpeg.sh:/app/pre-ffmpeg.sh:ro \

	# Custom commands to run in the container immediately
	# before attempting to connect to minetest
	-v `pwd`/pre-minetest.sh:/app/pre-minetest.sh:ro \

	# ======== ADVANCED/INTERNALS ========

	# Custom ffmpeg video filters
	-e vfilter= \
	# Custom ffmpeg audio filters
	-e afilter= \

	# Example dynamic filter: render contents of a notice file (crop the
	# trailing newline) in the bottom left, UTC timestamp in bottom right.
	#-e vfilter="drawtext='fontfile=/minetest/fonts/Arimo-Regular.ttf:fontcolor=white:fontsize=12:x=(w-text_w-4):y=(h-text_h-4):shadowx=1:shadowy=1:alpha=0.5:text=%{gmtime\:%Y-%m-%d %H\\\\\:%M\\\\\:%SZ}',drawtext='fontfile=/minetest/fonts/Arimo-Regular.ttf:fontcolor=white:fontsize=12:x=4:y=(h-text_h-4+lh):shadowx=1:shadowy=1:alpha=0.5:textfile=/app/notice.txt':reload=true" \
	#-v `pwd`/notice.txt:/app/notice.txt:ro \

	# Custom ffmpeg thread queue size
	# (Should not need to be adjusted)
	-e threadqueue=1024 \

	# Enable direct rendering access on Debian
	# FIXME: This doesn't seem to actually work yet!
	#--group-add 44 \
	#--group-add 107 \
	#--device /dev/dri:/dev/dri \

	warr1024/minetestcast:latest
```

## Building

To build locally, just `make build`.  Note that this includes downloading and compiling the Minetest client from source, which may take a long time.

## Docker Tags

- Tags will generally match a git ref (tag, branch, or hash) from Minetest, matching the version of the Minetest client included in that build.
- Tags do not necessarily indicate what version of the minetestcast scripts and infrastructure is included; you should generally only assume that the latest tag will have the current infrastructure.
- Only the docker tag matching latest MT git tag (e.g. `:5.6.1`) is guaranteed to be maintained, though even that may have some lag.
- The `:latest` docker tag itself should always be an alias to the latest minetest git tag (semver).
- Tags named after branches (e.g. `:master`) may be running development versions and be arbitrarily ahead of the latest stable release, or arbitrarily behind the current master.
