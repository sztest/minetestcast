#!/bin/sh
set -ex

exec Xvfb :0 -screen 0 "${width}x${height}x24"
