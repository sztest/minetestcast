FROM debian:bookworm-slim AS base

RUN apt-get update -y

RUN apt-get upgrade -y

########################################################################

FROM base AS download-mt

RUN apt-get install -y curl

WORKDIR /build/minetest
ARG MTVER
RUN curl -sL https://github.com/minetest/minetest/archive/${MTVER}.tar.gz \
	| tar xzf - --strip-components 1

########################################################################

FROM base AS download-irr

RUN apt-get install -y curl

WORKDIR /build/irrlicht
ARG MTVER
RUN curl -sLf https://raw.githubusercontent.com/minetest/minetest/${MTVER}/misc/irrlichtmt_tag.txt | tee irrtag
RUN if [ -s irrtag]; then curl -sLf https://github.com/minetest/irrlicht/archive/refs/tags/"`cat irrtag`".tar.gz \
	| tar xzf - --strip-components 1; fi

########################################################################

FROM base AS build

RUN apt-get install -y \
	cmake \
	build-essential \
	zlib1g-dev \
	libjpeg-dev \
	libpng-dev \
	libglx-dev \
	libgl-dev \
	libxxf86vm-dev \
	libfreetype-dev \
	libopenal-dev \
	libcurl4-openssl-dev \
	libvorbis-dev \
	libsqlite3-dev \
	libzstd-dev \
	libxi-dev \
	libsdl2-dev

WORKDIR /build/minetest
COPY --from=download-mt /build/minetest/ /build/minetest/
COPY --from=download-irr /build/irrlicht/ /build/minetest/lib/irrlichtmt/

RUN cmake \
	-DBUILD_CLIENT=1 \
	-DBUILD_SERVER=0 \
	-DRUN_IN_PLACE=1 \
	-DUSE_CURL=1 \
	-DUSE_LUAJIT=1 \
	-DFREETYPE_LIBRARY=/usr/lib/x86_64-linux-gnu/libfreetype.so.6 \
	.

RUN make -j4

########################################################################

FROM base AS stage

RUN apt-get install -y dbus

RUN mkdir -p /stage/var/lib/dbus
RUN dbus-uuidgen >/stage/var/lib/dbus/machine-id

COPY --from=download-mt /build/minetest /stage/minetest
COPY --from=build /build/minetest/bin /stage/minetest/bin

COPY --chown=1000:1000 ./ /stage/app/
RUN chown -R 1000:1000 /stage/app

########################################################################

FROM base AS final

ARG UID
RUN useradd -d /app -u 1000 app

RUN apt-get install -y \
	libc-bin \
	supervisor \
	xvfb \
	ffmpeg \
	pulseaudio \
	pulseaudio-utils \
	dbus \
	dbus-x11 \
	feh \
	libglx-mesa0 \
	libgl1-mesa-dri \
	libcurl4 \
	zlib1g \
	libjpeg62-turbo \
	libpng16-16 \
	libglx0 \
	libgl1 \
	libxxf86vm1 \
	libfreetype6 \
	libopenal1 \
	libvorbis0a \
	libsqlite3-0 \
	libsdl2-2.0 \
	nodejs \
	zstd \
	procps

WORKDIR /app
USER app

ENV width=1024
ENV height=576
ENV mtserver=172.17.0.1
ENV mtport=30000
ENV mtuser=stream
ENV mtpass=stream
ENV outformat=flv
ENV outuri=rtmp://172.17.0.1/live
ENV fftimeout=10
ENV fps=30
ENV vkeyint=60
ENV vfilter=
ENV afilter=
ENV threadqueue=1024
ENV vcrf=32
ENV vmaxrate=800k
ENV vmaxburst=1600k
ENV avbr=2

CMD ["/usr/bin/supervisord", "-c", "/app/supervisord.conf"]

COPY --from=stage /stage/ /
