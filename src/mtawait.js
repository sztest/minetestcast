#!/usr/bin/env node

const process = require('process');
const dgram = require('dgram');
const resolver = new (require('dns').promises.Resolver)();

const hostname = process.env.mtserver;
const port = Number(process.env.mtport) || 30000;

const hello = Buffer.from('4f45740300000001', 'hex');
const byepre = Buffer.from('4f457403', 'hex');
const byepost = Buffer.from('000003', 'hex');

async function ping() {
	const addr = /^(\d+\.){3}\d+$/.test(hostname) ? hostname : (await resolver.resolve4(hostname))[0];
	const sock = dgram.createSocket('udp4');
	await new Promise(r => sock.bind(r));
	await new Promise((r, j) => sock.send(hello, port, addr, e => e ? j(e) : r()));
	const msg = await new Promise((r, j) => {
		setTimeout(() => j('no server response'), 1000);
		sock.on('message', r);
	});
	if(!Buffer.isBuffer(msg) || (msg.length != 14))
		throw Error('invalid response message');
	const id = msg.slice(12, 14);
	await new Promise((r, j) => sock.send(byepre + id + byepost, port, addr, e => e ? j(e) : r()));
	await new Promise(r => sock.close(r));
}

async function pingloop() {
	try {
		await ping();
		process.exit(0);
	} catch(e) {
		console.log([new Date(), e]);
		setTimeout(pingloop, 1000);
	}
}

pingloop();
