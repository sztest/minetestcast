#!/bin/sh
set -ex

pulseaudio --daemonize=false --system=false &

sleep 2

pacmd load-module module-null-sink sink_name=loopback
pacmd set-default-sink loopback

wait