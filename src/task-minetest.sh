#!/bin/sh
set -ex

export DISPLAY=:0
export PULSE_LATENCY_MSEC=200

for x in 1 2 3 4 5 6 7 8; do
	pkill feh ||:
	feh --no-fehbg --bg-fill /app/backdrop.png &
	sleep 1
done

rm -rf /tmp/minetest
mkdir -p /tmp/minetest
cd /tmp/minetest

for x in /minetest/*; do
	ln -s "$x" .
done

rm -rf bin client
cp -R /minetest/bin /minetest/client .

(
	set -ex
	cat /app/minetest-base.conf
	echo # ensure line-end
	cat /app/minetest.conf
	echo # ensure line-end
	echo "fps_max = ${fps}"
	echo "fps_max_unfocused = ${fps}"
	echo "screen_w = ${width}"
	echo "screen_h = ${height}"
) | tee minetest.conf

if [ -x /app/pre-minetest.sh ]; then
	sh /app/pre-minetest.sh
fi

# node /app/mtawait.js

exec bin/minetest \
	--config /tmp/minetest/minetest.conf \
	--address "${mtserver}" \
	--port "${mtport}" \
	--name "${mtuser}" \
	--password "${mtpass}" \
	--go
