#!/bin/sh
set -ex

sleep 5

if [ -n "${vfilter}" ]; then vfilter=",${vfilter}"; fi
if [ -n "${afilter}" ]; then afilter=",${afilter}"; fi

if [ -x /app/pre-ffmpeg.sh ]; then
	sh /app/pre-ffmpeg.sh
fi

ffmpeg \
	-f x11grab \
	-draw_mouse 0 \
	-thread_queue_size "${threadqueue}" \
	-framerate "${fps}" \
	-i :0 \
	-i /app/overlay.png \
	-filter_complex "overlay=0:0${vfilter}" \
	-f pulse \
	-thread_queue_size "${threadqueue}" \
	-i loopback.monitor \
	-map 0:v \
	-map 2:a \
	-pix_fmt yuv420p \
	-c:v libx264 \
	-preset veryfast \
	-crf "${vcrf}" \
	-bufsize "${vmaxburst}" \
	-maxrate "${vmaxrate}" \
	-vsync cfr \
	-r "${fps}" \
	-x264opts "keyint=${vkeyint}:min-keyint=${vkeyint}:scenecut=-1" \
	-async 1 \
	-af "asetpts=PTS+2.1/TB,aresample${afilter}" \
	-c:a aac \
	-q:a "${avbr}" \
	-rw_timeout "${fftimeout}000000" \
	-f "${outformat}" \
	"${outuri}" \
2>&1 | tr \\r \\n
