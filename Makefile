MTVER=master
NAME=warr1024/minetestcast

build:
	env DOCKER_BUILDKIT=1 docker build \
		-t ${NAME}:latest \
		--build-arg MTVER=${MTVER} \
		src

pushall: push pushv

push: build
	docker push ${NAME}:latest

pushv: build
	docker tag ${NAME}:latest ${NAME}:${MTVER}
	docker push ${NAME}:${MTVER}
